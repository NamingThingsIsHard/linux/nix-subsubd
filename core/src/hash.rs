use anyhow::anyhow;
use std::fmt::{Display, Formatter};

/// base32 uses 5 bits to map to a char (2^5 = 32)
/// SHA256 is 256 bits --> 256 / 5 = 51.2
/// One extra char is needed to present that last bit
const SHA256_BASE32_LEN: u16 = 52;

/// A base32 string most likely to be a SHA256 hashsum
///
/// There are multiple alphabets for base32, so only the string length is of importance
///
/// The fields are read-only, otherwise one would have to sync them
/// and recheck on modification
#[derive(Eq, PartialEq, Hash, Clone)]
pub struct Sha256String {
    sha256: String,
    first_char: char,
}

impl Sha256String {
    pub fn sha256(&self) -> String {
        self.sha256.clone()
    }
    pub fn first_char(&self) -> char {
        self.first_char
    }
}

impl Display for Sha256String {
    fn fmt(&self, formatter: &mut Formatter<'_>) -> std::fmt::Result {
        formatter.write_str(self.sha256.as_str())
    }
}

impl TryFrom<String> for Sha256String {
    type Error = anyhow::Error;

    /// Only accept base32 encoded SHA256 hashsum strings
    fn try_from(value: String) -> Result<Self, Self::Error> {
        if value.len() != usize::from(SHA256_BASE32_LEN) {
            return Err(anyhow!(
                "Must be base32 encoded SHA256 with 64 characters: {} {}",
                value.len(),
                value
            ));
        }
        Ok(Sha256String {
            sha256: value.clone(),
            first_char: value
                .chars()
                .next()
                .ok_or(anyhow!("Couldn't get first character of {}", value))?,
        })
    }
}
