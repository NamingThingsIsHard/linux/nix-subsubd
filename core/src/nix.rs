use data_encoding::Encoding;
use data_encoding_macro::new_encoding;

pub const NIX_BASE32: Encoding = new_encoding! {
    // omitted: E O U T
    // taken from https://github.com/kolloch/nix-base32
    symbols: "0123456789abcdfghijklmnpqrsvwxyz",
};

/// A representation of the information retrieved from a binary cache
///
/// The narinfo crate has better information
/// TODO replace with struct from narinfo crate
#[derive(Eq, PartialEq, Debug)]
pub struct NarInfo {
    pub compression: String,
    pub file_hash: String,
    pub file_size: u32,
    pub nar_hash: String,
    pub nar_size: u32,
    pub store_path: String,
    pub url: String,
}

impl TryFrom<String> for NarInfo {
    type Error = String;

    /// Attempts to parse a simple colon separated string into NarInfo
    ///
    /// ```
    /// use nss_core::nix::NarInfo;
    /// assert_eq!(NarInfo::try_from(String::from(r#"
    ///   StorePath: /nix/store/znfrmqg6n8vl4dn0rk0w8ni875l58cmd-somebar-1.0.3
    ///   URL: nar/0zyyxzr58s161b35zmky658ziy30xqwavl2n320ivck8g9y99y40.nar.xz
    ///   Compression: xz
    ///   FileHash: sha256:0zyyxzr58s161b35zmky658ziy30xqwavl2n320ivck8g9y99y40
    ///   FileSize: 26192
    ///   NarHash: sha256:1p7y0nbmvi8j5qimdc0pqzs7jgbfysbshzpd208bcm26wzsxvkp9
    ///   NarSize: 85936
    ///   References: 31axfs6jsslijkdybyl3410zwfy1gvky-gcc-12.3.0-lib ...
    ///   Deriver: jmxycg5v28kdflgzmp7mx8rc72ih0gnl-somebar-1.0.3.drv
    ///   Sig: cache.nixos.org-1:+BASE64STRING
    /// "#)).unwrap(), NarInfo{
    ///     compression: String::from("xz"),
    ///     file_hash: String::from("sha256:0zyyxzr58s161b35zmky658ziy30xqwavl2n320ivck8g9y99y40"),
    ///     file_size: 26192,
    ///     nar_hash: String::from("sha256:1p7y0nbmvi8j5qimdc0pqzs7jgbfysbshzpd208bcm26wzsxvkp9"),
    ///     nar_size: 85936,
    ///     store_path: String::from("/nix/store/znfrmqg6n8vl4dn0rk0w8ni875l58cmd-somebar-1.0.3"),
    ///     url: String::from("nar/0zyyxzr58s161b35zmky658ziy30xqwavl2n320ivck8g9y99y40.nar.xz"),
    /// });
    /// assert_eq!(NarInfo::try_from(String::from(r#"
    ///   StorePath: /nix/store/znfrmqg6n8vl4dn0rk0w8ni875l58cmd-somebar-1.0.3
    ///   URL: nar/0zyyxzr58s161b35zmky658ziy30xqwavl2n320ivck8g9y99y40.nar.xz
    ///   FileHash: sha256:0zyyxzr58s161b35zmky658ziy30xqwavl2n320ivck8g9y99y40
    ///   FileSize: 26192
    ///   NarHash: sha256:1p7y0nbmvi8j5qimdc0pqzs7jgbfysbshzpd208bcm26wzsxvkp9
    ///   NarSize: 85936
    ///   References: 31axfs6jsslijkdybyl3410zwfy1gvky-gcc-12.3.0-lib ...
    ///   Deriver: jmxycg5v28kdflgzmp7mx8rc72ih0gnl-somebar-1.0.3.drv
    ///   Sig: cache.nixos.org-1:+BASE64STRING
    /// "#)).expect_err("Must have error"), String::from("Couldn't parse compression"));
    /// ```
    fn try_from(value: String) -> std::result::Result<Self, Self::Error> {
        let mut opt_compression: Option<String> = None;
        let mut opt_file_hash: Option<String> = None;
        let mut opt_file_size: Option<u32> = None;
        let mut opt_nar_hash: Option<String> = None;
        let mut opt_nar_size: Option<u32> = None;
        let mut opt_store_path: Option<String> = None;
        let mut opt_url: Option<String> = None;
        for line in value.clone().lines() {
            if let Some((key, value)) = line.split_once(": ") {
                let trimmed_key = key.trim();
                let trimmed_value = value.trim();
                let value_u32 = trimmed_value
                    .to_string()
                    .parse::<u32>()
                    .map_err(|error| format!("Cannot parse {}: {}", trimmed_value, error));
                match trimmed_key {
                    "Compression" => opt_compression = Some(String::from(trimmed_value)),
                    "FileHash" => opt_file_hash = Some(String::from(trimmed_value)),
                    "FileSize" => opt_file_size = Some(value_u32?),
                    "NarHash" => opt_nar_hash = Some(String::from(trimmed_value)),
                    "NarSize" => opt_nar_size = Some(value_u32?),
                    "StorePath" => opt_store_path = Some(String::from(trimmed_value)),
                    "URL" => opt_url = Some(String::from(trimmed_value)),
                    _ => {}
                }
            }
        }
        match (
            opt_compression,
            opt_file_hash,
            opt_file_size,
            opt_nar_hash,
            opt_nar_size,
            opt_store_path,
            opt_url,
        ) {
            (None, _, _, _, _, _, _) => Err(String::from("Couldn't parse compression")),
            (_, None, _, _, _, _, _) => Err(String::from("Couldn't parse file hash")),
            (_, _, None, _, _, _, _) => Err(String::from("Couldn't parse file size")),
            (_, _, _, None, _, _, _) => Err(String::from("Couldn't parse NAR hash")),
            (_, _, _, _, None, _, _) => Err(String::from("Couldn't parse NAR size")),
            (_, _, _, _, _, None, _) => Err(String::from("Couldn't parse store path")),
            (_, _, _, _, _, _, None) => Err(String::from("Couldn't parse URL")),
            (
                Some(compression),
                Some(file_hash),
                Some(file_size),
                Some(nar_hash),
                Some(nar_size),
                Some(store_path),
                Some(url),
            ) => Ok(NarInfo {
                compression,
                file_hash,
                file_size,
                nar_hash,
                nar_size,
                store_path,
                url,
            }),
        }
    }
}
