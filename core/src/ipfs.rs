use crate::nix::NIX_BASE32;
use data_encoding::HEXLOWER;

/// Prefix SHA256 with f01551220 to get a CID
///
/// https://matrix.to/#/!gWiVYCURkxIapWZaGy:ipfs.io/$_UyJlABj1oKVMW3AjmOHSFbVb83JipTSAcfI3sYTKWI?via=matrix.org&via=ipfs.io&via=crabsin.space
///
/// IPFS CIDs consist of multiple fields to indicate
///  - the encoding of the CID itself (base16,32,64,...) aka "multibase"
///  - the CID version (0 or 1 at the moment)
///  - what is being addressed (raw binary, raw git object, bitcoin block, ...) aka multicodec
///  - how it was hashed (SHA256, blake, ...) aka multihash
/// This is a
///  - f: multibase --> base16 encoded string
///  - 01: CID v1
///  - 55: multicodec 55 --> raw binary
///  - 12: multihash SHA256
///  - 20: multihash of length 0x20 bytes / 32 bytes
///
/// Example: https://cid.ipfs.tech/#f01551220169e3a927edf52bc535876bfd726aa0afa6ac679a1b1267388f28be42e4ba903
const SHA_256_CID_PREFIX: &str = "f01551220";

/// A very simplied Content ID builder for IPFS that build a base16 CID
///
/// [`sha256`] is nix-base32 encoded hashsum (nix decided to be special and use a different alphabet)
pub fn build_ipfs_cid(sha256: &String) -> anyhow::Result<String> {
    let base16_hashsum = HEXLOWER.encode(NIX_BASE32.decode(sha256.as_bytes())?.as_slice());
    Ok(format!("{SHA_256_CID_PREFIX}{base16_hashsum}"))
}
