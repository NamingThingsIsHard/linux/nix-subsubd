use std::fmt::{Display, Formatter};

use clap::ValueEnum;

pub mod hash;
pub mod ipfs;
pub mod mapper;
pub mod nix;

#[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord, ValueEnum)]
pub enum Protocol {
    Http,
    Https,
}

impl Display for Protocol {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let string = match self {
            Protocol::Http => "http",
            Protocol::Https => "https",
        };
        f.write_str(string)
    }
}
