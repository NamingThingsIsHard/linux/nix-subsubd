use serde::{Deserialize, Serialize};

/// Contains information about a [nss_mapper::Sha256String]
///
/// The value part of the key-value pair where key = [nss_mapper::Sha256String]
#[derive(Serialize, Deserialize)]
pub struct MappingValue {
    // TODO add optional NarInfo for storage to store the name of the file
    pub ipfs_cid: Option<String>,
}

impl MappingValue {
    /// Use fields from another mapping if there is no existing value in self
    pub fn merge(self, other: Self) -> Self {
        MappingValue {
            ipfs_cid: self.ipfs_cid.or(other.ipfs_cid),
        }
    }
}
