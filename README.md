nix-subsubd (nss) a proxy for nix binary cache substituters

At the moment, the only target for the binary cache is IPFS, but that can be expanded later.

**Status** This is idea is currently dead in the water due to a [bug in IPFS][ipfs_sha256]
hindering the retrieval of content from IPFS using just the content's SHA256 hash.

# How it works

[Substituters][substituters] allow `nix` to retrieve prebuilt packages 
and insert them into the store. The most used one is https://cache.nixos.org which is a 
centralized cache of build artifacts.
Build artifacts are stored as compressed (xz) NAR (Nix ARchive) files.

`nss` acts a proxy between `nix` and a binary HTTP cache. The flow is simple

1. `nix` requests information about the NARs (narinfo) from the binary cache
   1. `nss` forwards that to binary cache
   1. binary cache responds
   1. `nss` stores the information in an SQLite db, with the pertinent information
   being the SHA256 hashsum of the compressed NAR file (`.nar.xz`)
1. `nix` requests the `.nar.xz`
   1. `nss` tries to retrieve file from IPFS **fails**
   1. if file isn't on IPFS `nss` forwards request to binary cache **to be implemented**

## What doesn't work

Retrieval of content by converting SHA256 hash to a Content ID (CID) doesn't work,
due to a [bug in IPFS][ipfs_sha256]. Until that's solved or an alternative for IPFS is found
`nss` won't work.

## How to test

 - Optional: Start IPFS locally
   - Alternatively, find an IPFS gateway e.g ipfs.io
 - Start `nss` with `cargo run`
   - Use `--help` to find out how to configure it
 - Find a hash from your /nix/store/ you'd like to test
 - Get NAR informfation and store to SQLite `curl localhost:3000/$hash.narinfo`
   - Note down the value of the `URL:` field
 - Retrieve the compressed NAR from IPFS `curl localhost:3000/$URL` (or `wget`)
   - if it doesn't work, you can see the logs of `nss` where the IPFS URL is printed\
     and give it a shot to see what the response from IPFS is

# Name?

- Full name: nix substituter substituter daemon
- Meaning: nix substituter proxy daemon

Proxy is just another word for substitute :)

[ipfs_sha256]: https://github.com/ipfs/kubo/issues/10426
[substituters]: https://nixlang.wiki/en/nix/internals/substituters
