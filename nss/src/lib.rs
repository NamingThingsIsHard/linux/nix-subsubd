use std::path::Path;

use anyhow::{anyhow, Context};
use sqlx::migrate::MigrateDatabase;
use sqlx::{Pool, Row, Sqlite, SqlitePool};

use nss_core::hash::Sha256String;
use nss_core::nix::NarInfo;

pub mod backends;
pub mod cli;
pub mod mappers;

/// Creates an object used to interact with an SQL database
pub async fn create_sqlite_pool(db_path: &String) -> anyhow::Result<Pool<Sqlite>> {
    let sqlite_url = format!("sqlite:{}", db_path);
    let sqlite_url_str = sqlite_url.as_str();
    if !Path::new(db_path).exists() {
        Sqlite::create_database(sqlite_url_str).await?;
    }
    let pool = SqlitePool::connect(sqlite_url_str).await?;
    create_tables(&pool).await?;
    Ok(pool)
}

/// Creates the necessary tables in SQLite
///
/// At the moment that means only the NAR info retrieved from the binary cache
/// To ensure the same NAR info isn't inserted multiple times, it's deduped by the store_path
/// which should be unique
pub async fn create_tables(pool: &Pool<Sqlite>) -> anyhow::Result<()> {
    let mut conn = pool.acquire().await?;
    sqlx::query(
        r#"CREATE TABLE IF NOT EXISTS narinfo (
        id              INTEGER PRIMARY KEY NOT NULL,
        compression     TEXT NOT NULL,
        file_hash       TEXT NOT NULL,
        file_size       INTEGER NOT NULL,
        nar_hash        TEXT NOT NULL,
        nar_size        INTEGER NOT NULL,
        store_path      TEXT UNIQUE NOT NULL,
        url             TEXT NOT NULL
    );"#,
    )
    .execute(&mut *conn)
    .await?;
    Ok(())
}

/// Write the NAR info to the database
///
/// It shouldn't be a problem if the entry already exists as it will just be ignored
/// Information shouldn't be updated
pub async fn store_nar_info(pool: &Pool<Sqlite>, nar_info: &NarInfo) -> anyhow::Result<()> {
    let mut conn = pool.acquire().await?;
    sqlx::query(
        r#"INSERT OR IGNORE INTO narinfo
    ( compression, file_hash, file_size, nar_hash, nar_size, store_path, url )
    VALUES (
        $1, $2, $3, $4, $5, $6, $7
    );"#,
    )
    .bind(nar_info.compression.clone())
    .bind(nar_info.file_hash.clone())
    .bind(nar_info.file_size)
    .bind(nar_info.nar_hash.clone())
    .bind(nar_info.nar_size)
    .bind(nar_info.store_path.clone())
    .bind(nar_info.url.clone())
    .execute(&mut *conn)
    .await?;
    Ok(())
}

/// Retrieves the SHA256 hashsum of the .nar.xz corresponding to a store path
/// as indicated by a previous stored NAR info
pub async fn query_file_hash(
    pool: &Pool<Sqlite>,
    store_hash: &String,
) -> anyhow::Result<Sha256String> {
    let mut conn = pool.acquire().await?;
    let url = format!("nar/{store_hash}.nar.xz");
    let row = sqlx::query(
        r#"
        SELECT file_hash
        FROM narinfo
        WHERE
            url = $1
        "#,
    )
    .bind(url.clone())
    .fetch_one(&mut *conn)
    .await
    .context(anyhow!("Cannot find URL {}", url))?;

    let hash_w_prefix: String = row.try_get("file_hash")?;
    let (hash_type, hash) = hash_w_prefix
        .split_once(':')
        .ok_or(anyhow::anyhow!("Cannot determine hash type"))?;
    if hash_type == "sha256" {
        Sha256String::try_from(hash.to_string())
    } else {
        Err(anyhow!("Unsupported hash_type: {}", hash_type))
    }
}
