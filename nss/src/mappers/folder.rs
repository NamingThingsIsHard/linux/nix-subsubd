use std::fs;
use std::path::PathBuf;

use nss_core::hash::Sha256String;
use nss_core::mapper::MappingValue;

use crate::mappers::traits::Mapper;

/// Use a folder structure to retrieve information about SHA256 strings
pub struct FolderMapper {
    pub folder: PathBuf,
}

impl Mapper for FolderMapper {
    fn map(self, sha256: &Sha256String) -> Result<MappingValue, anyhow::Error> {
        let path = self
            .folder
            .join(sha256.first_char().to_string())
            .join(format!("{}.yaml", sha256.sha256()));
        Ok(serde_yaml::from_str::<MappingValue>(
            fs::read_to_string(path)?.as_str(),
        )?)
    }
}
