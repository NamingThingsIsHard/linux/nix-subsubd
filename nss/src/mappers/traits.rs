use nss_core::hash::Sha256String;
use nss_core::mapper::MappingValue;

/// Allowing mapping one value to another so that a [Backend] can retrieve data from a source
pub trait Mapper {
    /// SHA256 strings
    fn map(self, sha256: &Sha256String) -> anyhow::Result<MappingValue>;
}
