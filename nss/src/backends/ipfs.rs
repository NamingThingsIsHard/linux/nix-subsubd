use anyhow::anyhow;
use futures::StreamExt;
use futures_core::Stream;
use reqwest::redirect::Policy;
use reqwest::Client;

use nss_core::hash::Sha256String;
use nss_core::Protocol;

use crate::backends::traits::Backend;
use crate::mappers::traits::Mapper;

pub struct IpfsBackend {
    pub gateway_host: String,
    pub gateway_port: u16,
    pub gateway_protocol: Protocol,
}

impl Backend for IpfsBackend {
    /// Attempts to retrieve data behind a SHA256 string from IPFS
    async fn stream(
        self,
        sha256: Sha256String,
        mapper: impl Mapper,
    ) -> anyhow::Result<impl Stream<Item = anyhow::Result<bytes::Bytes>>> {
        // Get CID from mapper if possible
        let mapping_value = mapper.map(&sha256)?;
        let ipfs_cid = mapping_value
            .ipfs_cid
            .ok_or(anyhow!("Mapping has no CID"))?;

        // Get data from IPFS
        let ipfs_url = format!(
            "{}://{}:{}/ipfs/{}?download=true",
            self.gateway_protocol, self.gateway_host, self.gateway_port, ipfs_cid
        );
        let client = Client::builder()
            // Set the maximum number of redirects to follow
            .redirect(Policy::limited(5))
            .build()?;
        let ipfs_response = client.get(ipfs_url).send().await?.error_for_status()?;
        let response_stream = ipfs_response.bytes_stream();
        // reqwest::Error cannot be automatically converted to anyhow::Error
        // gotta do it with StreamExt!
        let anyhow_response_stream =
            response_stream.map(|result| result.map_err(|e| anyhow!(e.to_string())));
        Ok(anyhow_response_stream)
    }
}
