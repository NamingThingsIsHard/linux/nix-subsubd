use crate::mappers::traits::Mapper;
use futures_core::Stream;
use nss_core::hash::Sha256String;

pub trait Backend {
    /// Attempts to get the data behind a SHA256 string
    #[allow(async_fn_in_trait)]
    async fn stream(
        self,
        sha256: Sha256String,
        mapper: impl Mapper,
    ) -> anyhow::Result<impl Stream<Item = anyhow::Result<bytes::Bytes>>>;
}
