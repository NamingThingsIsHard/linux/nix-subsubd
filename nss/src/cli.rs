use clap::{Args, Parser};
use nss_core::Protocol;
use std::fmt;

/// nss is a substituter proxy for nix
///
/// It maps SHA256 hashes of NAR files to interim values.
/// These interim values are then used by a backend to retrieve the NAR from an alternative source.
///
/// Supported mappers are: **FolderMapper**
///
/// Supported backends are: IPFS
#[derive(Parser, Debug, Clone)]
#[command(version, about)]
pub struct Config {
    #[command(flatten)]
    pub common: CommonConfig,

    // Backends
    #[command(flatten)]
    pub backends: Backends,
    #[command(flatten)]
    pub backend_config_ipfs: BackendConfigIpfs,

    // Mappers
    #[command(flatten)]
    pub mappers: Mappers,
    #[command(flatten)]
    pub mapper_config_folder: MapperConfigFolder,
}

#[derive(Args, Debug, Clone)]
#[group(multiple = true)]
pub struct CommonConfig {
    /// Which host interface to use
    #[arg(long, default_value = "127.0.0.1")]
    pub host: String,
    /// Which port to use
    /// Under 1024 will probably require admin rights
    #[arg(short, long, default_value_t = 3000)]
    pub port: u16,
    /// Where the SQLite db is stored
    #[arg(long, default_value = "/tmp/nss.db")]
    pub db: String,

    /// DNS or IP address of the cache to proxy
    /// This will be inserted into an URI for making the HTTP request
    #[arg(long, default_value = "cache.nixos.org")]
    pub cache_host: String,
    /// Which port to use when querying the cache
    #[arg(long)]
    pub cache_port: Option<u16>,
    /// DNS or IP address of the cache to proxy
    /// This will be inserted into an URI for making the HTTP request
    #[arg(long, value_enum, default_value_t = Protocol::Https)]
    pub cache_protocol: Protocol,
}

/// Creates a [Arg][clap::Arg] used to select an [ArgGroup][clap::ArgGroup] from a mutally exclusive
/// list of groups.
///
/// The user will be able to use something like `--group1` or `--group2`.
///
/// Appropriate arg groups have to be created separately and select separately.
/// In order to do so, this also creates an enum with a [TryFrom] implementation.
macro_rules! group_flag {
    (
        $struct_name:ident,
        $struct_doc: literal,
        $enum_name:ident,
        $field:ident, $field_doc:literal, $variant:ident,
        $($field_tail: ident, $field_tail_doc:literal, $variant_tail:ident),*
    ) => {
        #[derive(Args, Debug, Clone)]
        #[group(multiple = false)]
        #[doc = $struct_doc]
        pub struct $struct_name {
            #[arg(long)]
            #[doc = $field_doc]
            $field : bool,
            $(
                #[arg(long)]
                #[doc = $field_tail_doc]
                $field_tail: bool
            )*
        }

        pub enum $enum_name {
            $variant,
            $($variant_tail),*
        }

        impl TryFrom<$struct_name> for $enum_name {
            type Error = String;

            fn try_from(value: $struct_name) -> std::result::Result<Self, Self::Error> {
                if value. $field {
                    Ok($enum_name::$variant)
                }
                $(
                else if value. $field_tail {
                    Ok($enum_name::$variant_tail)
                }
                )*
                else {
                    Err(format!("{:?} cannot be converted to {}", value, stringify!($enum_name)))
                }
            }
        }

        impl fmt::Display for $enum_name {
            fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
                let string = match self {
                    $enum_name::$variant => stringify!($varant)
                    $(
                    $enum_name::$variant_tail => stringify!($variant_tail)
                    )*
                };
                f.write_str(string)
            }
        }
    };
}

group_flag!(
    Backends,
    "Selects the backend from which to get the NAR",
    SelectedBackend,
    backend_ipfs,
    "Enables the IPFS backend",
    Ipfs,
);

#[derive(Args, Debug, Clone)]
#[group(multiple = true, requires = "backend_ipfs")]
pub struct BackendConfigIpfs {
    /// DNS or IP address of the IPFS API to query
    /// This will be inserted into an URI for making the HTTP request
    #[arg(long, default_value = "localhost")]
    pub backend_ipfs_host: String,
    /// Which port to use when querying the IPFS API
    #[arg(long, default_value_t = 8080)]
    pub backend_ipfs_port: u16,
    #[arg(long, value_enum, default_value_t = Protocol::Http)]
    pub backend_ipfs_protocol: Protocol,
}

group_flag!(
    Mappers,
    "Selects how to map a NAR's SHA256 to a value usable by a backend",
    SelectedMapper,
    mapper_folder,
    "Uses the [FolderMapper]",
    Folder,
);

#[derive(Args, Debug, Clone)]
#[group(multiple = true, requires = "mapper_folder")]
pub struct MapperConfigFolder {
    /// Folder containing the mappings
    #[arg(long)]
    pub mapper_folder_folder: String,
}
