use std::io::{Error, ErrorKind};
use std::path::PathBuf;

use actix_web::http::StatusCode;
use actix_web::web::Data;
use actix_web::{error, get, web, App, HttpResponse, HttpResponseBuilder, HttpServer, Result};
use anyhow::anyhow;
use clap::Parser;
use log::info;
use sqlx::SqlitePool;

use nss::backends::ipfs::IpfsBackend;
use nss::backends::traits::Backend;
use nss::cli::{Config, SelectedBackend, SelectedMapper};
use nss::mappers::folder::FolderMapper;
use nss::{create_sqlite_pool, query_file_hash, store_nar_info};
use nss_core::nix::NarInfo;

const VERSION: &str = env!("CARGO_PKG_VERSION");

struct AppState {
    config: Config,
    pool: SqlitePool,
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    // Help output isn't perfect due to https://github.com/clap-rs/clap/issues/4191
    // Doesn't output the names of the arg groups
    let config = Config::parse();
    let config_clone = config.clone();
    let pool = create_sqlite_pool(&config.common.db.clone())
        .await
        .map_err(|e| Error::new(ErrorKind::Other, e.to_string()))?;

    let app_data_config = Data::new(AppState {
        config: config.clone(),
        pool,
    });
    HttpServer::new(move || {
        App::new()
            .app_data(app_data_config.clone())
            .service(root)
            .service(nix_cache_info)
            .service(proxy_narinfo)
            .service(proxy_nar_file)
    })
    .bind((config_clone.common.host.clone(), config_clone.common.port))?
    .run()
    .await
}

#[get("/")]
async fn root() -> Result<String> {
    Ok(format!("nix-subsub v{}", VERSION))
}

#[get("/nix-cache-info")]
async fn nix_cache_info() -> Result<&'static str> {
    Ok("StoreDir: /nix/store\nWantMassQuery: 1\nPriority: 40")
}

#[get("/{hash}.narinfo")] // <- define path parameters
async fn proxy_narinfo(path: web::Path<String>, data: Data<AppState>) -> Result<String> {
    let hash = path.into_inner();
    let cache_url = format!(
        "{}://{}/{}.narinfo",
        data.config.common.cache_protocol, data.config.common.cache_host, hash
    );
    // TODO query DB before cache
    let reqwest_err_mapper = |err: reqwest::Error| error::ErrorBadGateway(err.to_string());
    let cache_response = reqwest::get(cache_url)
        .await
        .map_err(reqwest_err_mapper)?
        .error_for_status()
        .map_err(reqwest_err_mapper)?;
    let response_text = cache_response.text().await.map_err(reqwest_err_mapper)?;

    let nar_info = NarInfo::try_from(response_text.clone()).map_err(error::ErrorBadGateway)?;
    store_nar_info(&data.pool, &nar_info)
        .await
        .map_err(|e| error::ErrorInternalServerError(e.to_string()))?;
    Ok(response_text)
}
#[get("/nar/{hash}.nar.xz")] // <- define path parameters
async fn proxy_nar_file(path: web::Path<String>, data: Data<AppState>) -> Result<HttpResponse> {
    let nar_response = get_nar(&path, &data).await;
    match nar_response {
        Ok(streaming_response) => Ok(streaming_response),

        // Fallback to the binary cache when an error occurs
        Err(err) => {
            let fallback_url = format!(
                "{}://{}/nar/{}.nar.xz",
                data.config.common.cache_protocol, data.config.common.cache_host, path
            );
            info!("Falling back to {} after {}", fallback_url.clone(), err);
            let stream = reqwest::get(fallback_url)
                .await
                .map_err(|cid_error| error::ErrorInternalServerError(cid_error.to_string()))?
                .error_for_status()
                .map_err(|cid_error| error::ErrorInternalServerError(cid_error.to_string()))?
                .bytes_stream();
            Ok(HttpResponseBuilder::new(StatusCode::OK).streaming(stream))
        }
    }
}

async fn get_nar(hash: &String, data: &Data<AppState>) -> anyhow::Result<HttpResponse> {
    let config = data.config.clone();
    let nar_sha256 = query_file_hash(&data.pool, hash).await?;

    // Construct backend
    let selected_backend =
        SelectedBackend::try_from(config.backends.clone()).map_err(|e| anyhow!(e))?;
    let backend = match selected_backend {
        SelectedBackend::Ipfs => IpfsBackend {
            gateway_host: config.backend_config_ipfs.backend_ipfs_host,
            gateway_port: config.backend_config_ipfs.backend_ipfs_port,
            gateway_protocol: config.backend_config_ipfs.backend_ipfs_protocol,
        },
    };
    // Construct mapper
    let selected_mapper =
        SelectedMapper::try_from(config.mappers.clone()).map_err(|e| anyhow!(e))?;
    let mapper = match selected_mapper {
        SelectedMapper::Folder => FolderMapper {
            folder: PathBuf::from(config.mapper_config_folder.mapper_folder_folder),
        },
    };
    // Get data from backend
    let stream = backend.stream(nar_sha256.clone(), mapper).await?;
    info!(
        "Streaming data from {} for SHA256 {}",
        selected_backend, nar_sha256
    );
    Ok(HttpResponseBuilder::new(StatusCode::OK).streaming(stream))
}
