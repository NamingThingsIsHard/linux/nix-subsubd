use std::fs;
use std::io::Read;
use std::path::{Path, PathBuf};

use anyhow::{anyhow, Context};
use clap::Parser;
use log::{debug, info, warn};
use simplelog::*;

use nss_core::hash::Sha256String;
use nss_core::mapper::MappingValue;
use nss_mapper::cli::{Cli, Commands, SourceType};
use nss_mapper::hashers::ipfs::IpfsHasher;
use nss_mapper::hashers::traits::Hasher;
use nss_mapper::sources::store::StoreSource;

type Sha256ToContentIterator =
    Box<dyn Iterator<Item = anyhow::Result<Option<(Sha256String, Box<dyn Read + Send>)>>>>;

fn main() -> anyhow::Result<()> {
    let args = Cli::parse();
    SimpleLogger::init(args.log_level, Config::default())?;
    let sha256_to_content_iterator: Sha256ToContentIterator = match args.source_type {
        SourceType::StoreFolder => {
            info!("Using store as a source");
            Box::new(StoreSource::new(&args.source)?)
        }
    };
    let hasher: &dyn Hasher = &(match args.command {
        Commands::IPFS(ipfs_args) => {
            info!("Using IPFS for hashing");
            IpfsHasher::from(ipfs_args)
        }
    });

    write_mappings(
        sha256_to_content_iterator.map(move |x| hash(x, hasher)),
        PathBuf::from(args.target_folder).as_path(),
    )?;

    Ok(())
}

/// Run through the iterator generated by a [SourceType] to map [Sha256String]s to [MappingValue]
fn hash(
    to_hash: anyhow::Result<Option<(Sha256String, Box<dyn Read + Send>)>>,
    hasher: &dyn Hasher,
) -> anyhow::Result<Option<(Sha256String, MappingValue)>> {
    match to_hash {
        Ok(option) => {
            if let Some((sha256, reader)) = option {
                debug!("Added SHA256");
                Ok(Some((sha256, hasher.hash(reader)?)))
            } else {
                Ok(None)
            }
        }
        Err(err) => Err(err),
    }
}

/// Write each mapping out to the target folder
fn write_mappings(
    mappings: impl Iterator<Item = anyhow::Result<Option<(Sha256String, MappingValue)>>>,
    target_folder: &Path,
) -> anyhow::Result<()> {
    if target_folder.is_file() {
        return Err(anyhow!(
            "{:?} already exists as a file. Must be a folder",
            target_folder
        ));
    }
    for result in mappings {
        match result {
            Ok(Some((sha256, value))) => match write_mapping(target_folder, &sha256, value) {
                Ok(path) => {
                    info!("Wrote mapping to {:?}", path);
                }
                Err(err) => {
                    warn!("Error writing {}: {}", sha256.sha256(), err);
                }
            },
            Ok(None) => {}
            Err(err) => {
                warn!("Could not process an item: {:?}", err)
            }
        }
    }
    Ok(())
}

/// Write the value to `target_folder/sha256[0]/sha256.yaml`
fn write_mapping(
    target_folder: &Path,
    sha256: &Sha256String,
    value: MappingValue,
) -> anyhow::Result<PathBuf> {
    let folder = target_folder.join(sha256.first_char().to_string());
    fs::create_dir_all(folder.clone()).context(anyhow!("Could not create {:?}", folder.clone()))?;

    // Merge with existing file if one exists
    let file_path = folder.join(format!("{}.yaml", sha256.sha256()));
    let value_to_write = if file_path.is_file() {
        let existing_value: MappingValue =
            serde_yaml::from_str(fs::read_to_string(file_path.clone())?.as_str())?;
        value.merge(existing_value)
    } else {
        value
    };
    let yaml_content = serde_yaml::to_string(&value_to_write)?;
    fs::write(file_path.as_path(), yaml_content)?;
    Ok(file_path)
}
