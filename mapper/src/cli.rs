use crate::hashers::ipfs::IpfsHasher;
use clap::{Args, Parser, Subcommand, ValueEnum};
use log::LevelFilter;
use nss_core::Protocol;
use std::fmt::{Display, Formatter};

/// Download stuff then add to IPFS and store the CID in the following structure
///
/// Everything is stored in a folder structure in order to be diffable
///
/// - root
///   * 0
///   * ...
///   * 9
///   * a
///     - $sha256.yaml
///   * b
///   * ...
///   * z
#[derive(Parser)]
#[command(version, about, long_about)]
#[command(propagate_version = true)]
pub struct Cli {
    #[command(subcommand)]
    pub command: Commands,
    /// Which host interface to use
    pub source: String,
    /// Which host interface to use
    pub target_folder: String,

    /// What source is
    #[arg(long="source", value_enum, default_value_t = SourceType::StoreFolder)]
    pub source_type: SourceType,

    #[arg(long, default_value_t = LevelFilter::Info)]
    pub log_level: LevelFilter,
}

#[derive(Subcommand)]
pub enum Commands {
    /// Hash downloaded files using IPFS
    IPFS(IpfsArgs),
}

#[derive(Args)]
pub struct IpfsArgs {
    /// DNS or IP address of the IPFS API to query
    /// This will be inserted into an URI for making the HTTP request
    #[arg(long, default_value = "localhost")]
    ipfs_host: String,
    /// Which port to use when querying the IPFS API
    #[arg(long, default_value_t = 5001)]
    ipfs_port: u16,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord, ValueEnum)]
pub enum SourceType {
    /// A nix store folder
    StoreFolder,
}

impl Display for SourceType {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let string = match self {
            SourceType::StoreFolder => "store",
        };
        f.write_str(string)
    }
}

impl From<IpfsArgs> for IpfsHasher {
    fn from(args: IpfsArgs) -> Self {
        IpfsHasher {
            scheme: Protocol::Http,
            host: args.ipfs_host,
            port: args.ipfs_port,
        }
    }
}
