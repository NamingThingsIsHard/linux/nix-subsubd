use nss_core::mapper::MappingValue;
use std::io::Read;

pub trait Hasher {
    fn hash(&self, reader: Box<dyn Read + Send>) -> anyhow::Result<MappingValue>;
}
