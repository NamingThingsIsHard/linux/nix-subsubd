use anyhow::{anyhow, Context};
use std::io::Read;

use crate::hashers::traits::Hasher;
use nss_core::mapper::MappingValue;
use nss_core::Protocol;
use reqwest::blocking::multipart::{Form, Part};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
struct AddResponse {
    #[serde(rename = "Hash")]
    pub hash: String,
    #[serde(rename = "Name")]
    pub name: String,
    #[serde(rename = "Size")]
    pub size: String,
}

pub struct IpfsHasher {
    pub scheme: Protocol,
    pub host: String,
    pub port: u16,
}

impl Hasher for IpfsHasher {
    /// Uploads file to IPFS for hashing and retrieves the CID
    fn hash(&self, reader: Box<dyn Read + Send>) -> anyhow::Result<MappingValue> {
        let url = format!("{}://{}:{}/api/v0/add", self.scheme, self.host, self.port);
        let form = Form::new().part("file", Part::reader(reader));
        let response = reqwest::blocking::Client::new()
            .post(url)
            .multipart(form)
            .send()?
            .error_for_status()?;
        let text = response.text()?;

        let add_response: AddResponse =
            serde_json::from_str(text.as_str()).context(anyhow!("Got response {:?}", text))?;

        Ok(MappingValue {
            ipfs_cid: Some(add_response.hash),
        })
    }
}
