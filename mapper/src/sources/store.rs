use std::fs;
use std::fs::{DirEntry, ReadDir};
use std::io::Read;
use std::path::PathBuf;

use anyhow::{anyhow, Context};
use log::{debug, info, warn};
use nss_core::hash::Sha256String;
use nss_core::nix::NarInfo;
use reqwest::blocking::Client;

pub const STORE_DIGEST_LENGTH: u8 = 32;
pub struct StoreSource {
    paths: ReadDir,
}

impl StoreSource {
    pub fn new(source: &String) -> anyhow::Result<Self> {
        Ok(StoreSource {
            paths: fs::read_dir(PathBuf::from(source))?,
        })
    }

    fn handle(
        &self,
        entry: &DirEntry,
    ) -> anyhow::Result<Option<(Sha256String, Box<dyn Read + Send>)>> {
        info!("Store is handling {:?}", entry.path());
        let entry_context = format!("entry is {:?}", entry.path());
        let file_name_orig = entry.file_name();
        let file_name = file_name_orig
            .to_str()
            .ok_or(anyhow!(
                "Couldn't get directory name of {:?}",
                file_name_orig
            ))
            .context(entry_context.clone())?;
        // Get digest of storepath
        // https://github.com/NixOS/nix/blob/master/doc/manual/src/protocols/store-path.md
        // digest = base-32 representation of the first 160 bits of a SHA-256 hash of fingerprint
        let split_option = file_name.split_once('-');
        if split_option.is_none() {
            return Ok(None);
        }
        let (item_digest, _) = split_option
            .ok_or(anyhow!("Couldn't split {}", file_name))
            .context(entry_context.clone())?;

        // The digest must be 32 chars longs
        if item_digest.len() != usize::from(STORE_DIGEST_LENGTH) {
            warn!(
                "Digest not {} chars: {} {:?}",
                STORE_DIGEST_LENGTH,
                item_digest.len(),
                entry
            );
            return Ok(None);
        }
        // TODO Pass binary cache as params
        // TODO extract function
        let url = format!("https://cache.nixos.org/{}.narinfo", item_digest);
        let client = Client::new();
        let narinfo_result = client.get(url.clone()).send()?.error_for_status();
        if let Err(status_error) = narinfo_result {
            warn!("Ignoring {:?}", entry.path());
            warn!("Cannot retrieve narinfo {}: {}", url, status_error);
            return Ok(None);
        }
        let narinfo_str = narinfo_result?.text()?;
        let narinfo = NarInfo::try_from(narinfo_str).map_err(|e| anyhow!(e))?;
        debug!("Nar info for {:?}: {:?}", entry, narinfo);

        // file_hash is the hash of the file that's downloaded
        // nar_hash of the file one decompressed
        // absolutely bizarre naming
        // we want the hash of the file that's downloaded!
        let (hash_type, nar_hash) = narinfo
            .file_hash
            .split_once(':')
            .ok_or(anyhow!(
                "Expected string like 'sha256:$hash': {}",
                narinfo.file_hash
            ))
            .context(entry_context.clone())?;

        if hash_type != "sha256" {
            return Err(
                anyhow!("Expecting hash type sha256: {}", hash_type).context(entry_context.clone())
            );
        }
        let sha256 = Sha256String::try_from(nar_hash.to_string()).context(entry_context.clone())?;
        info!("NAR SHA256 of {:?}: {}", entry.path(), sha256);
        // Download the file from the cache in streaming mode
        let nar_response = Box::new(
            client
                .get(format!("https://cache.nixos.org/{}", narinfo.url))
                .send()?
                .error_for_status()?,
        );
        debug!("Streaming NAR of {:?}", entry.path());
        Ok(Some((sha256, nar_response)))
    }
}

impl Iterator for StoreSource {
    type Item = anyhow::Result<Option<(Sha256String, Box<dyn Read + Send>)>>;

    fn next(&mut self) -> Option<Self::Item> {
        match self.paths.next() {
            None => None,
            Some(entry_result) => match entry_result {
                Ok(dir_entry) => Some(self.handle(&dir_entry)),
                Err(err) => Some(Err(anyhow::Error::new(err))),
            },
        }
    }
}
